from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect

######## REDIRECT HOME ##########
def redirect_to_recipe_list(request):
    return redirect("recipe_list")

urlpatterns = [
    path("", redirect_to_recipe_list, name="homepage"),
    path('admin/', admin.site.urls),
    path('recipes/', include('recipes.urls')),
    path("accounts/", include("accounts.urls")),
]