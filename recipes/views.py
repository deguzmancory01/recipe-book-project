from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required     #pulls the login_reguired function from the django.contrib.auth.decorators module

##### IF USER LOGGED IN, VIEW RECIPE LIST #####
@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipes_list": recipes,
    }
    return render(request, "recipes/list.html", context)

######### CREATE RECIPE ##########
@login_required                             # adds authentication to the next function
def create_recipe(request):                 # create a function to create a recipe
    if request.method == "POST":            # if the request is an HTTP POST Method: 
        form = RecipeForm(request.POST)         # view the form with editable information
        if form.is_valid():                 # If all fields of the form are filled out properly:
            recipe = form.save(False)           # return the form that has not been saved in the database yet
            recipe.author = request.user        # make the authenticated (logged in) user the author
            recipe.save()                       # save the form
            return redirect("recipe_list")      # return to recipe list, after saving
    else:
        form = RecipeForm()                 # else return an empty form
        
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

######### EDIT RECIPE ##########
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
        
    context = {
        "recipe_object": recipe,
        "post_form": form,
    }
    return render(request, "recipes/edit.html", context)

######### RECIPE LIST ##########
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipes": recipes,
    }
    
    return render(request, "recipes/list.html", context)

######### HOMEPAGE ##########
def homepage(request):
    recipes = Recipe.objects.all()
    context = {
        "recipes": recipes,
    }
    
    return render(request, "recipes/homepage.html", context)

######### SHOW RECIPE ##########
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context =  {
        "recipe": recipe,
    }
    return render(request, 'recipes/detail.html', context)