from django.contrib import admin
from recipes.models import Recipe, RecipeStep

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "title",
    ]
    

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "instruction",
        "order",
        "id",
    )